# definitions.py
# CREATED ON: 27.II.2024
# AUTHOR: okryp@codeberg.org
# In here variables that would clutter up main.py
# Mostly initial states for before the game starts

from lib.Classes import *

# by how much each card in a column gets displaced downwards
ROW_DISPLACE_MODIFIER = 2

# Determines where the card will be rendered
locations = (
    # COLUMNS OF UNSORTED CARDS
    "COL 0",			# 0
    "COL 1",			# 1
    "COL 2",			# 2
    "COL 3",			# 3
    "COL 4",			# 4
    "COL 5",			# 5
    "COL 6",			# 6
    
    # DRAW PILE
    "STACK",			# 7
    
    # DRAWN PILE
    "UNCOVERED STACK",	# 8
    
	# PILES FOR SORTED CARDS
    "S PILE",			# 9
    "H PIlE",			# 10
    "D PILE",			# 11
    "C PILE",			# 12
)

gamestates = (
    "BOOT",  			# Startup
    "MENU",  			# Menu screen
    "GAME",  			# In game screen
    "OVER",  			# Game over display 
    "KILL",				# Closing
)

# Un-shuffled initial deck
deck = [
	Card(1, 	"Spades"),
	Card(2, 	"Spades"),
	Card(3, 	"Spades"),
	Card(4, 	"Spades"),
	Card(5, 	"Spades"),
	Card(6, 	"Spades"),
	Card(7, 	"Spades"),
	Card(8, 	"Spades"),
	Card(9, 	"Spades"),
	Card(10, 	"Spades"),
	Card(11, 	"Spades"),
	Card(12, 	"Spades"),
	Card(13, 	"Spades"),
	Card(1, 	"Hearts"),
	Card(2, 	"Hearts"),
	Card(3, 	"Hearts"),
	Card(4, 	"Hearts"),
	Card(5, 	"Hearts"),
	Card(6, 	"Hearts"),
	Card(7, 	"Hearts"),
	Card(8, 	"Hearts"),
	Card(9, 	"Hearts"),
	Card(10, 	"Hearts"),
	Card(11, 	"Hearts"),
	Card(12, 	"Hearts"),
	Card(13, 	"Hearts"),
	Card(1, 	"Diamonds"),
	Card(2, 	"Diamonds"),
	Card(3, 	"Diamonds"),
	Card(4, 	"Diamonds"),
	Card(5, 	"Diamonds"),
	Card(6, 	"Diamonds"),
	Card(7, 	"Diamonds"),
	Card(8, 	"Diamonds"),
	Card(9, 	"Diamonds"),
	Card(10, 	"Diamonds"),
	Card(11, 	"Diamonds"),
	Card(12, 	"Diamonds"),
	Card(13, 	"Diamonds"),
	Card(1, 	"Clubs"),
	Card(2, 	"Clubs"),
	Card(3, 	"Clubs"),
	Card(4, 	"Clubs"),
	Card(5, 	"Clubs"),
	Card(6, 	"Clubs"),
	Card(7, 	"Clubs"),
	Card(8, 	"Clubs"),
	Card(9, 	"Clubs"),
	Card(10, 	"Clubs"),
	Card(11, 	"Clubs"),
	Card(12, 	"Clubs"),
	Card(13, 	"Clubs"),
]

# Verified works :D
gameboard = [[None] * 13] * 7

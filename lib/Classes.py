# Here be classes of various sorts


# Class used specifically for the need of placing strings in specific locations
# on the screen when using addstr() (for some reason addstr() puts y first, but
# i am not about to change gods given order)
class Positioned_String:
	def __init__(self, x: int, y: int, string: str):
		self.x = x
		self.y = y
		self.string = string


# Class used mostly to store where what is stored on the screen
class Vector:
	def __init__(self, x: int, y: int):
		self.x = x
		self.y = y


# Class that makes the magic happen. Class that allows storing the suit, value
# and revealed status of each card in the game. Probably gonna need to add some 
# more tweaks laters
class Card:
	def __init__(self, value: int, suit: str):
		self.value = value
		self.suit = suit

	revealed: bool = False

	# See definitions.py
	location: int
	
	def reveal_card(self):
		self.revealed = True


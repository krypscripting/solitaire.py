# Here be functions of larger proportions

from random import *
from lib.Classes import *
from lib.Letters import Letter

# Shuffles the deck (obvious)
def shuffle_cards(deck: list) -> list:
	shuffle(deck)
	return deck


# Determines which side of the card to draw, and returns an array of strings 
# ready with the coordinates to which they go to
def front_or_back(initial_position: Vector ,card: Card):
	if card.revealed:
		return [
			Positioned_String(initial_position.x, initial_position.y,
			"#######"),
			Positioned_String(initial_position.x, initial_position.y + 1,
			f"#{card.value} | {card.suit[0]}#"),
			Positioned_String(initial_position.x, initial_position.y + 2,
			"#  |  #"),
			Positioned_String(initial_position.x, initial_position.y + 3,
			"#--+--#"),
			Positioned_String(initial_position.x, initial_position.y + 4,
			"#  |  #"),
			Positioned_String(initial_position.x, initial_position.y + 5,
			f"#{card.suit[0]} | {card.value}#"),
			Positioned_String(initial_position.x, initial_position.y + 6,
			"#######"),
		]
	else:
		return [
			Positioned_String(initial_position.x, initial_position.y,
			"#######"),
			Positioned_String(initial_position.x, initial_position.y + 1,
			"#@X@X@#"),
			Positioned_String(initial_position.x, initial_position.y + 2,
			"#X@X@X#"),
			Positioned_String(initial_position.x, initial_position.y + 3,
			"#@X@X@#"),
			Positioned_String(initial_position.x, initial_position.y + 4,
			"#X@X@X#"),
			Positioned_String(initial_position.x, initial_position.y + 5,
			"#@X@X@#"),
			Positioned_String(initial_position.x, initial_position.y + 6,
			"#######"),	
		]


# Function that puts the cards in the right spot for the beginning of the game
def organise_board(deck: list, gameboard: list):
	gameboard[0][0] = deck[0]
	gameboard[1][0] = deck[1]
	gameboard[2][0] = deck[2]
	gameboard[3][0] = deck[3]
	gameboard[4][0] = deck[4]
	gameboard[5][0] = deck[5]
	gameboard[6][0] = deck[6]
	gameboard[1][1] = deck[7]
	gameboard[2][1] = deck[8]
	gameboard[3][1] = deck[9]
	gameboard[4][1] = deck[10]
	gameboard[5][1] = deck[11]
	gameboard[6][1] = deck[12]
	gameboard[2][2] = deck[13]
	gameboard[3][2] = deck[14]
	gameboard[4][2] = deck[15]
	gameboard[5][2] = deck[16]
	gameboard[6][2] = deck[17]
	gameboard[3][3] = deck[18]
	gameboard[4][3] = deck[19]
	gameboard[5][3] = deck[20]
	gameboard[6][3] = deck[21]
	gameboard[4][4] = deck[22]
	gameboard[5][4] = deck[23]
	gameboard[6][4] = deck[24]
	gameboard[5][5] = deck[25]
	gameboard[6][5] = deck[26]
	gameboard[6][6] = deck[27]

	for i, col in enumerate(gameboard):
		for card in col:
			card.location = i


	deck = deck[28:]


# * An actually finished function (let's hope it is at least)
def draw_card(screen, position: Vector, card: Card):
	# card.reveal_card()
	lines: list = front_or_back(position, card)
	for string in lines:
		screen.addstr(string.y, string.x, string.string)


# TODO
def draw_menu(screen):
	title = "solitaire"
	initial_position = Vector(1, 1)
	title = []
	for idx, l in title:
		letter = Letter(
			Vector(
				initial_position.x + idx * Letter.letter_width,
				initial_position.y + idx * Letter.letter_width
			),
			l
		)

		title.append(letter.return_PS(l, letter.position))

	screen.clear()

	for lines in title:
		for string in lines:
			screen.addstr(string.y, string.x, string.string)


# TODO
def draw_game(screen):
	pass


# TODO
def draw_gameover(screen):
	pass


# TODO
def key_event_handler(gamestate):
	pass



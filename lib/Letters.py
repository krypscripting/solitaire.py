# Letters.py
# Here be positioned strings (see Classes.py) for the main menu

from lib.Classes import Positioned_String, Vector


class Letter:
    letter_width = 5

    position: Vector
    letter: str

    a = [
        "AAAAA",
        "A   A",
        "AAAAA",
        "A   A",
        "A   A",
    ]
    b = [
        "BBBB ",
        "B   B",
        "BBBB ",
        "B   B",
        "BBBB ",
    ]
    c = [
        " CCCC",
        "C    ",
        "C    ",
        "C    ",
        " CCCC",
    ]
    d = [
        "DDDD ",
        "D   D",
        "D   D",
        "D   D",
        "DDDD ",
    ]
    e = [
        "EEEEE",
        "E    ",
        "EEE  ",
        "E    ",
        "EEEEE",
    ]
    f =[
        "FFFFF",
        "F    ",
        "FFF  ",
        "F    ",
        "F    ",
    ]
    g = [
        " GGGG",
        "G    ",
        "G  GG",
        "G   G",
        " GGGG",
    ]
    h = [
        "H   H",
        "H   H",
        "HHHHH",
        "H   H",
        "H   H",
    ]
    i = [
        " I ",
        "   ",
        " I ",
        " I ",
        " I ",
    ]
    j = [
        "JJJJJ",
        "    J",
        "    J",
        "J   J",
        " JJJ ",
    ]
    k = [
        "K   K",
        "K KK ",
        "KK   ",
        "K KK ",
        "K   K",
    ]
    l = [
        "L    ",
        "L    ",
        "L    ",
        "L    ",
        "LLLLL",
    ]
    m = [
        "M   M",
        "MM MM",
        "M M M",
        "M   M",
        "M   M",
    ]
    n = [
        "N   N",
        "NN  N",
        "N N N",
        "N  NN",
        "N   N",
    ]
    o = [
        " OOO ",
        "O   O",
        "O   O",
        "O   O",
        " OOO ",
    ]
    p = [
        "PPPP ",
        "P   P",
        "PPPP ",
        "P    ",
        "P    ",
    ]
    q = [
        " QQQ ",
        "Q   Q",
        "Q   Q",
        "Q Q Q",
        " QQQ ",
    ]
    r = [
        "RRRR ",
        "R   R",
        "RRRR ",
        "R  R ",
        "R   R",
    ]
    s = [
        " SSSS",
        "S    ",
        " SSS ",
        "    S",
        "SSSS ",
    ]
    t = [
        "TTTTT",
        "  T  ",
        "  T  ",
        "  T  ",
        "  T  ",
    ]
    u = [
        "U   U",
        "U   U",
        "U   U",
        "U   U",
        " UUU ",
    ]
    v = [
        "V   V",
        "V   V",
        " V V ",
        " V V ",
        "  V  ",
    ]
    w = [
        "W   W",
        "W   W",
        "W W W",
        "WW WW",
        "W   W",
    ]
    x = [
        "X   X",
        " X X ",
        "  X  ",
        " X X ",
        "X   X",
    ]
    y = [
        "Y   Y",
        " Y Y ",
        "  Y  ",
        "  Y  ",
        "  Y  ",
    ]
    z = [
        "ZZZZZ",
        "   Z ",
        "  Z  ",
        " Z   ",
        "ZZZZZ",
    ]

    def __init__(self, position: Vector, letter: str):
        self.position = position
        self.letter = letter


    def return_PS(self, char: str, position: Vector):
        rv = []
        letter = getattr(self, char.lower[0])

        for row in letter:
            rv.append(Positioned_String(
                position.x,
                position.y,
                row
            ))
        
        return rv

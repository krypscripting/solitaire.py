# main.py 
# Created on: 27.II.2024
# Author: okryp@codeberg.org
# Personal notes in ./personal_notes.md

import curses
from lib.Classes import *
from lib.Definitions import *
from lib.Functions import *

card_width: int = 7
card_height: int = 5
state = 0
board = gameboard

def draw(screen, gamestate):
    screen.clear()
    if gamestate == 0:
        screen.addstr("STARTING THE GAME")
        gamestate = 1
    if gamestate == 1:
        draw_menu(screen)
    if gamestate == 2:
        draw_gameover(screen)
    if gamestate == 3:
        screen.addstr("END SCREEN")



def main():
    # Init
    screen = curses.initscr()
    curses.noecho()
    curses.cbreak()
    screen.keypad(True)

    # Draw loop
    while True:
        draw(screen, state)
        if screen.getch() == ord("q"):
            break
            
    # Exit
    curses.nocbreak()
    screen.keypad(False)
    curses.echo()
    curses.endwin()

    exit(0)


main()
